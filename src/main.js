document.addEventListener('DOMContentLoaded', function() {
  const htmlElem = ` <section class="Box3">
    <h1 class="Title">Join Our Program</h1>
    <h3 class="Text">Sed do eiusmod tempor incididunt <br> ut labore et dolore magna aliqua.</h3>
    <div class="email-container">
      <input type="email" id="email-input" placeholder="Email">
      <button type="submit">Subscribe</button>
    </div>
  </section>`;

  const footer = document.querySelector('footer');
  footer.insertAdjacentHTML('beforebegin', htmlElem);

  const submit = document.querySelector('button[type="submit"]');
  submit.addEventListener('click', function(e) {
    e.preventDefault();
    console.log(document.querySelector("#email-input").value);
  });
});
